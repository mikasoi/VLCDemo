﻿#include "Widget.h"

#include <QVBoxLayout>

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    this->resize(800, 550);

    VLCPlayer *pWidget = new VLCPlayer(this);

    QVBoxLayout *pLayout = new QVBoxLayout;
    pLayout->setMargin(0);
    pLayout->setSpacing(0);
    pLayout->addWidget(pWidget);
    this->setLayout(pLayout);
}

Widget::~Widget()
{

}
