﻿#include "Widget.h"
#include <QApplication>
#include <QFile>

//设置qss样式
void setQss(const QString &style)
{
    QFile qssFile(style);
    qssFile.open(QFile::ReadOnly);
    qApp->setStyleSheet(qssFile.readAll());
    qssFile.close();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    setQss(":/qss/custom.qss");

    Widget w;
    w.show();

    return a.exec();
}
