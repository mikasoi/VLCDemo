#-------------------------------------------------
#
# Project created by QtCreator 2021-07-21T09:34:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(vlc/VLCPlayer.pri)

TARGET      = VLCDemo
TEMPLATE    = app
MOC_DIR     = temp/moc
RCC_DIR     = temp/rcc
UI_DIR      = temp/ui
OBJECTS_DIR = temp/obj
DESTDIR     = $$PWD/bin


SOURCES += main.cpp\
        Widget.cpp

HEADERS  += Widget.h

FORMS    +=

RESOURCES += \
    images.qrc

DISTFILES +=
