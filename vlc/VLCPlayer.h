﻿#ifndef VLCPLAYER_H
#define VLCPLAYER_H

#include <QWidget>
#include <QBoxLayout>

#include <QToolButton>

class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;
class VlcWidgetVideo;
class VlcWidgetVolumeSlider;
class VlcWidgetSeek;

#define BUTTON_STYLESHEET   "QToolButton{\
                             background: transparent;\
                             width: 28px;\
                             height: 28px;\
                             qproperty-iconSize: 28px 28px;\
                             border: 0px;}"

#define BUTTON_STYLESHEET2  "QToolButton{\
                             background: transparent;\
                             width: 20px;\
                             height: 20px;\
                             qproperty-iconSize: 20px 20px;\
                             border: 0px;}"

// VLC播放器
class VLCPlayer : public QWidget
{
    Q_OBJECT
public:
    explicit                VLCPlayer(QWidget *parent = nullptr);
    ~VLCPlayer();

    void                    initVideoWidget();
    void                    initControlWidget();
    void                    initLayout();

signals:

public slots:
    void                    openLocal();
    void                    openUrl();
    void                    pauseOrResume();
    void                    reachEnd();
    void                    stop();
    void                    sound();

private:
    VlcInstance *           m_pInstance;
    VlcMedia *              m_pMedia;
    VlcMediaPlayer *        m_pMediaPlayer;
    VlcWidgetVideo *        m_pVideoWidget;
    VlcWidgetVolumeSlider * m_pVolume;
    VlcWidgetSeek *         m_pSeek;

    QWidget *               m_pControlWidget;
    QVBoxLayout *           m_pControlLayout;
    QToolButton *           m_pOpenLocalBtn;
    QToolButton *           m_pOpenUrlBtn;
    QToolButton *           m_pPlayBtn;
    QToolButton *           m_pStopBtn;
    QToolButton *           m_pSoundBtn;

    bool                    m_bIsPlay;
};

#endif // VLCPLAYER_H
