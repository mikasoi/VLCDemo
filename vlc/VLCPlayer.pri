HEADERS += \
    $$PWD/VLCPlayer.h


SOURCES += \
    $$PWD/VLCPlayer.cpp


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lVLCQtCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lVLCQtCored
else:unix: LIBS += -L$$PWD/lib/ -lVLCQtCore

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lVLCQtWidgets
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lVLCQtWidgetsd
else:unix: LIBS += -L$$PWD/lib/ -lVLCQtWidgets

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include
