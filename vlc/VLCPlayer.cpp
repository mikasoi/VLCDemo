﻿#include "VLCPlayer.h"
#include "VLCQtCore/Common.h"
#include "VLCQtCore/Instance.h"
#include "VLCQtCore/Media.h"
#include "VLCQtCore/MediaPlayer.h"
#include "VLCQtWidgets/WidgetVideo.h"
#include "VLCQtWidgets/WidgetVolumeSlider.h"
#include "VLCQtWidgets/WidgetSeek.h"

#include <QFileDialog>
#include <QInputDialog>
#include <QApplication>
#include <QDebug>

#define CONTROL_HEIGHT          80

VLCPlayer::VLCPlayer(QWidget *parent) : QWidget(parent)
{
    this->resize(800, 600);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(QString::fromLocal8Bit("VLC播放器"));

    initVideoWidget();
    initControlWidget();
    initLayout();

    m_pMedia = NULL;
    m_bIsPlay = false;
}

VLCPlayer::~VLCPlayer()
{
    if (m_pMedia)
    {
        delete m_pMedia;
        stop();
    }
    delete m_pMediaPlayer;
    delete m_pInstance;
    delete m_pVolume;
    delete m_pSeek;
}

void VLCPlayer::initVideoWidget()
{
    m_pVideoWidget = new VlcWidgetVideo(this);
    m_pVideoWidget->setStyleSheet("QWidget { background: rgb(0, 0, 0); }");
//    QPalette palette(m_pSeek->palette());
//    palette.setColor(QPalette::Background, Qt::black);
//    m_pSeek->setAutoFillBackground(true);
//    m_pSeek->setPalette(palette);

    QStringList args = VlcCommon::args();
    m_pInstance = new VlcInstance(args, this);
    m_pMediaPlayer = new VlcMediaPlayer(m_pInstance);
    m_pVolume = new VlcWidgetVolumeSlider(this);
    m_pVolume->setOrientation(Qt::Horizontal); // 水平方向
    m_pVolume->setFixedSize(100, 20);
    m_pSeek = new VlcWidgetSeek(this);
    m_pSeek->setAutoHide(true);

    m_pMediaPlayer->setVideoWidget(m_pVideoWidget);
    m_pVideoWidget->setMediaPlayer(m_pMediaPlayer);
    m_pVolume->setMediaPlayer(m_pMediaPlayer);
    m_pVolume->setVolume(50);
    m_pSeek->setMediaPlayer(m_pMediaPlayer);

    connect(m_pMediaPlayer, SIGNAL(end()), this, SLOT(reachEnd()));
}

void VLCPlayer::initControlWidget()
{
    m_pControlWidget = new QWidget(this);
    m_pControlWidget->setMaximumHeight(CONTROL_HEIGHT);

    m_pOpenLocalBtn = new QToolButton(this);
    m_pOpenLocalBtn->setIcon(QIcon(":/images/Local.png"));
    m_pOpenLocalBtn->setToolTip(QString::fromLocal8Bit("打开本地视频"));
    m_pOpenLocalBtn->setStyleSheet(BUTTON_STYLESHEET);
    connect(m_pOpenLocalBtn, SIGNAL(clicked(bool)), this, SLOT(openLocal()));

    m_pOpenUrlBtn = new QToolButton(this);
    m_pOpenUrlBtn->setIcon(QIcon(":/images/Url.png"));
    m_pOpenUrlBtn->setToolTip(QString::fromLocal8Bit("打开视频连接"));
    m_pOpenUrlBtn->setStyleSheet(BUTTON_STYLESHEET);
    connect(m_pOpenUrlBtn, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    m_pPlayBtn = new QToolButton(this);
    m_pPlayBtn->setIcon(QIcon(":/images/Play.png"));
    m_pPlayBtn->setToolTip(QString::fromLocal8Bit("播放"));
    m_pPlayBtn->setStyleSheet(BUTTON_STYLESHEET);
    connect(m_pPlayBtn, SIGNAL(clicked(bool)), this, SLOT(pauseOrResume()));

    m_pStopBtn = new QToolButton(this);
    m_pStopBtn->setIcon(QIcon(":/images/Close.png"));
    m_pStopBtn->setToolTip(QString::fromLocal8Bit("关闭"));
    m_pStopBtn->setStyleSheet(BUTTON_STYLESHEET);
    connect(m_pStopBtn, SIGNAL(clicked(bool)), this, SLOT(stop()));

    m_pSoundBtn = new QToolButton(this);
    m_pSoundBtn->setIcon(QIcon(":/images/Sound.png"));
    m_pSoundBtn->setStyleSheet(BUTTON_STYLESHEET);
    connect(m_pSoundBtn, SIGNAL(clicked(bool)), this, SLOT(sound()));

    m_pControlLayout = new QVBoxLayout(m_pControlWidget);

    QHBoxLayout *pHLayout = new QHBoxLayout;
    pHLayout->setContentsMargins(10, 0, 10, 0);
    pHLayout->setSpacing(10);
    pHLayout->addWidget(m_pOpenLocalBtn);
    pHLayout->addWidget(m_pOpenUrlBtn);
    pHLayout->addWidget(m_pPlayBtn);
    pHLayout->addWidget(m_pStopBtn);
    pHLayout->addStretch();
    pHLayout->addWidget(m_pSoundBtn);
    pHLayout->addWidget(m_pVolume);

    m_pControlLayout->addWidget(m_pSeek);
    m_pControlLayout->addLayout(pHLayout);
}

void VLCPlayer::initLayout()
{
    QVBoxLayout *pVLayout = new QVBoxLayout(this);
    pVLayout->setMargin(0);
    pVLayout->setSpacing(0);
    pVLayout->addWidget(m_pVideoWidget);
    pVLayout->addWidget(m_pControlWidget);

    m_pVideoWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

void VLCPlayer::openLocal()
{
    QString strFile = QFileDialog::getOpenFileName(this,
                   QString::fromLocal8Bit("打开本地视频"), qApp->applicationDirPath() + "/../movie/", QString::fromLocal8Bit("MP4 (*.mp4);;多媒体文件(*)"));
    if (!strFile.isEmpty())
    {
        if (m_pMedia)
        {
            delete m_pMedia;
            m_pMedia = NULL;
        }
        m_pMedia = new VlcMedia(strFile, true, m_pInstance);
        m_pMediaPlayer->open(m_pMedia);

        m_pPlayBtn->setIcon(QIcon(":/images/Pause.png"));
        m_bIsPlay = true;
    }
}

void VLCPlayer::openUrl()
{
    // CCTV1 // http://39.134.66.66/PLTV/88888888/224/3221225816/index.m3u8
    // CCTV6 // http://39.134.66.66/PLTV/88888888/224/3221225814/index.m3u8
    QString strUrl = QInputDialog::getText(this,
                  QString::fromLocal8Bit("打开视频链接"), QString::fromLocal8Bit("URL: "),
                  QLineEdit::Normal,
                  "http://39.134.66.66/PLTV/88888888/224/3221225814/index.m3u8");
    if (!strUrl.isEmpty())
    {
        if (m_pMedia)
        {
            delete m_pMedia;
            m_pMedia = NULL;
        }
        m_pMedia = new VlcMedia(strUrl, m_pInstance);
        m_pMediaPlayer->open(m_pMedia);

        m_pPlayBtn->setIcon(QIcon(":/images/Pause.png"));
        m_bIsPlay = true;
    }
}

void VLCPlayer::pauseOrResume()
{
    if (m_bIsPlay)
    {
        m_pMediaPlayer->pause();
        m_pPlayBtn->setIcon(QIcon(":/images/Play.png"));
        m_pPlayBtn->setToolTip(QString::fromLocal8Bit("播放"));
    }
    else
    {
        m_pMediaPlayer->resume();
        m_pPlayBtn->setIcon(QIcon(":/images/Pause.png"));
        m_pPlayBtn->setToolTip(QString::fromLocal8Bit("暂停"));
    }

    m_bIsPlay = !m_bIsPlay;
}

void VLCPlayer::reachEnd()
{
    if (m_bIsPlay)
    {
        m_pPlayBtn->setIcon(QIcon(":/images/Play.png"));
        m_bIsPlay = false;
    }
}

void VLCPlayer::stop()
{
    m_pMediaPlayer->stop();
    reachEnd();
}

void VLCPlayer::sound()
{
    if (m_pVolume->mute())
    {
        m_pVolume->setMute(false);
        m_pSoundBtn->setIcon(QIcon(":/images/Sound.png"));
    }
    else
    {
        m_pVolume->setMute(true);
        m_pSoundBtn->setIcon(QIcon(":/images/Mute.png"));
    }
}
